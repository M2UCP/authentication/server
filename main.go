package main

import (
	"./server"
	"fmt"
	"math/rand"
	"os"
	"time"
)

func main( ) {
	// seed rand
	rand.Seed( time.Now( ).UnixNano( ) )

	// load configuration
	if configuration, err := server.BuildConfiguration( ); err == nil {
		// build server
		if _, err := server.BuildServer( configuration ); err == nil {
			for {
				time.Sleep( time.Second )
			}
		} else {
			fmt.Println(err)
			os.Exit(1)
		}
	} else {
		fmt.Println( err )
		os.Exit( 1 )
	}
}
