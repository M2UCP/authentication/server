Server
======

Introduction
------------

Server is responsible for identification of the user, and authentication.

It will receive and check user information.

It also has to authenticate itself to the incoming clients for them to be
sure to send sensible data to the right destination.

Dependencies
------------

```bash
go get gitlab.com/M2UCP/authentication/authenticator
go get gitlab.com/M2UCP/authentication/smartcard
```

Configuration
-------------

```json
{
	"server": {
		"port": 8080,
		"session": {
			"length": 64,
			"keepAlive": 20
		},
		"databasePath": "../database/"
	}
}
```

|name|type|description|default value|
|----|----|-----------|-------------|
|server.port|int|the port server will be listening on|8080|
|server.session.idLength|int|the session id string length|64|
|server.session.keepAlive|int|the session delay before being closed (seconds)|20|

Rapport
-------

Afin de réaliser la partie authentification M2M, nous avons choisi de nous baser sur l'algorithme TOTP,
pour Time-based One Time Password, particulièrement adapté ici du fait de la sensibilité des informations
devant transiter (dans le cadre de notre scénario).

Nous avons attribué un token privé au serveur, et un token privé identique à tous les clients.
Le token identique à tous les clients est une piste d'élément à modifier, comme elle ne permet
pas d'attester qu'un client est bien celui qu'il prétend être, mais ici, nous ne cherchons qu'à
vérifier si la carte est bien autorisée dans le système (plus une authentification qu'une identification).

Un token OTP est une chaîne de caractères de taille 16 composée de caractères valide dans la table des
caractères autorisés par l'encodage Base32 ([A-Z]u[2-7]).
En se basant sur ce token et sur le temps actuel (avec un intervalle donné), il est possible de générer un
mot de passe valide sur l'intervalle choisi. Ce code à est composé de 9 chiffres, et c'est lui qui circulera
entre les machines.

Le token est pour nous généré sur la base d'un rand.Seed( time.Now( ).UnixNano( ) ) correspondant au temps écoulé
en nanosecondes depuis le 1er janvier 1970.

Dans les cartes clients et serveurs (dédiées au M2M), nous retrouvons dans la partie

```raw
User Data 1

Card type     [0xCC=M2M Client/0x5E=M2M Server/0xC1=Client|Checksum Token Serveur|Checksum Token Client|Card mode again (same as 10)]
                                  10                                11                     12                          13
  
Token serveur [6OS4|ASS2|WC2N|U6KT]
                14   15   16   17
				
Token client  [RMKH|MXUB|7W5K|LSU3]
                18   19   20   21
```

Les caractères '|' indiquent simplement la limite des mots dans la mémoire de la carte.

L'idée est que lorsque qu'un client contacte le serveur, il lui fournisse son OTP actuel basé sur son token client privé.
Le serveur, si il réussit à générer le même OTP avec le token privé client qu'il possède, renvoie un OTP basé sur son token serveur
privé.
Si le client génère le même OTP que celui envoyé par le serveur grâce au token privé serveur, l'échange d'informations est par la suite
possible.


Author
------

SOARES Lucas <lucas.soares.npro@gmail.com>

https://gitlab.com/M2UCP/authentication/server.git
