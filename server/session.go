package server

import (
	"gitlab.com/M2UCP/authentication/authenticator"
	"time"
)

const(
	FlagSessionCardValid = 0x00000001
	FlagSessionLoginValid = 0x00000002
	FlagSessionBioValid = 0x00000004
)

// this is a session client, which is a client terminal connecting to server
type Session struct {
	// session id
	Id string

	// remote address
	RemoteAddress string

	// keep alive last contact time
	KeepAliveTime time.Time

	// is valid?
	IsValid bool

	// hash seed couples
	QPad, OPad, IPad authenticator.HashPad

	// login status
	loginStatus int
}

// build session
func BuildSession( id string,
	remoteAddress string ) *Session {
	// allocate
	session := &Session{
		Id:            id,
		RemoteAddress: remoteAddress,
		KeepAliveTime: time.Now( ),
		IsValid:       true,
	}

	// generate random pad
	session.IPad.Generate( )
	session.OPad.Generate( )
	session.QPad.Generate( )

	// done
	return session
}

// close session
func ( session *Session ) Close( ) {
	session.IsValid = false
}