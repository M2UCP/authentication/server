package server

import (
	"./database"
	"fmt"
	"gitlab.com/M2UCP/authentication/authenticator"
	"math/rand"
	"net"
	"net/http"
	"time"
)

// handle hello request
func ( server *Server ) handleHelloRequest( rw http.ResponseWriter,
	request *http.Request ) {
	// extract OTP from headers
	if receivedOtp := request.Header.Get( authenticator.OTPHTTPHeader ); len( receivedOtp ) <= 0 {
		rw.WriteHeader( http.StatusBadRequest )
	} else {
		if generatedOtp, err := server.tokenManager.GetServerOTP( ); err == nil {
			if generatedOtp  == receivedOtp {
				fmt.Println( "[SERVER] was given a proper hello (client OTPs match [",
					generatedOtp,
					"])" )
				if generatedOtp, err := server.tokenManager.GetClientOTP( ); err == nil {
					// add a session
					session := server.addSession( request.RemoteAddr )

					// notify
					fmt.Println( "[SERVER] client",
						request.RemoteAddr,
						"trusted, now sending server OTP",
						generatedOtp,
						"with session ID",
						session.Id,
						"and OPAD/IPAD",
						session.OPad,
						"/",
						session.IPad )

					// fill response headers
					rw.Header().Add( authenticator.OTPHTTPHeader,
						generatedOtp )
					rw.Header( ).Add( authenticator.SessionIDHTTPHeader,
						session.Id )
					rw.Header( ).Add( authenticator.OPadHTTPHeader,
						string( session.OPad ) )
					rw.Header( ).Add( authenticator.IPadHTTPHeader,
						string( session.IPad ) )

				} else {
					fmt.Println( err )
				}
			} else {
				fmt.Println( "[SERVER] bad OTP: ",
					generatedOtp,
					"!=",
					receivedOtp )
				rw.WriteHeader( http.StatusUnauthorized )
			}
		} else {
			rw.WriteHeader( http.StatusExpectationFailed )
		}
	}
}

// handle be right back (keepalive) requests
func ( server *Server ) handleBrbRequest( rw http.ResponseWriter,
	request *http.Request ) {
	// extract hostname
	hostname, _, _ := net.SplitHostPort( request.RemoteAddr )

	// extract session
	if sentSessionId := request.Header.Get( authenticator.SessionIDHTTPHeader ); len( sentSessionId ) > 0 {
		// look for session id
		if session, ok := server.sessionList[ sentSessionId ]; ok &&
			session.IsValid {
			if hostname == session.RemoteAddress {
				// update last contact time
				session.KeepAliveTime = time.Now()

				// notify
				fmt.Println("[SERVER] got valid keep alive request from",
					hostname,
					"(session",
					sentSessionId +
					")")
			} else {
				// bad hostname
				rw.WriteHeader( http.StatusConflict )

				// notify
				fmt.Println( "[SERVER] hostname",
					hostname,
					"tried to renew keepalive of session id",
					sentSessionId,
					"while session was initiated by",
					session.RemoteAddress,
					": denied" )
			}
		} else {
			// not authorized
			rw.WriteHeader( http.StatusUnauthorized )

			// notify
			fmt.Println( "[SERVER] hostname",
				hostname,
				"tried to renew an unknown or invalid session id",
				sentSessionId )
		}
	} else {
		// bad request
		rw.WriteHeader( http.StatusBadRequest )

		// notify
		fmt.Println( "[SERVER] hostname",
			hostname,
			"asked for session keep alive renewal without session ID" )
	}
}

// new user form
const NewUserFormPage =
`<html>
	<head>
	</head>
	<body>
		<h1>New user</h1>
		<p>You are about to add a new user to the system.</p>
		<form action="/new" method="post">
			Login: <input type="text" name="login" /><br />
			Password: <input type="password" name="password" /><br />
			Name: <input type="text" name="name" /><br />
			Card pin: <input type="number" name="pin" /><br />
			<input type="submit" />
		</form>

		<p>After click on submit, please insert the card which will be given to user</p>
	</body>
</html>`

// extract form value
func ExtractFormValue( request *http.Request,
	fieldName string ) string {
	if valueList := request.Form[ fieldName ]; valueList != nil &&
		len( valueList ) > 0 {
		return valueList[ 0 ]
	} else {
		return ""
	}
}

// generate random password
func GenerateRandomPassword( ) string {
	password := ""
	for i := 0; i < authenticator.CardPasswordLength; i++ {
		password += string( database.AuthorizedCharacterList[ rand.Int( )%len( database.AuthorizedCharacterList ) ] )
	}
	return password
}

// handle new user request
func ( server *Server ) handleNewUserRequest( rw http.ResponseWriter,
	request *http.Request ) {
	// extract hostname
	hostname, _, _ := net.SplitHostPort( request.RemoteAddr )

	// check hostname
	if hostname == "127.0.0.1" {
		switch request.Method {
		default:
			fallthrough
		case "GET":
			_, _ = rw.Write( [ ]byte( NewUserFormPage ) )
			break

		case "POST":
			if err := request.ParseForm( ); err == nil {
				// extract user data
				name := ExtractFormValue( request,
					"name" )
				login := ExtractFormValue( request,
					"login" )
				password := ExtractFormValue( request,
					"password" )
				cardPin := ExtractFormValue( request,
					"pin" )

				// assert data
				if len( name ) > 0 &&
					len( login ) > 0 &&
					len( password ) > 0 &&
					len( cardPin ) == 4 {
					// add user
					user := server.database.AddUser( name,
						login,
						password )

					// notify
					fmt.Println( "[SERVER - NEW USER INTERFACE] please insert card to write on" )

					// add card
					userCard := server.database.AddCardForUser( user.ID,
						GenerateRandomPassword( ) )

					// wait for card input
					for {
						server.cardHandler.Lock( )
						if card := server.cardHandler.GetLastConnectedCard( ); card != nil {
							// write data on card
							_ = card.SendUpdate( 0x10,
								[ ]byte{
									byte( authenticator.CardModeClient ),
									0,
									0,
									byte( authenticator.CardModeClient ),
								} )
							_ = card.SendUpdate( 0x14,
								[ ]byte( userCard.ID ) )
							_ = card.SendUpdate( 0x18,
								[ ]byte( userCard.Password ) )
							_ = card.SendUpdate( 0x38,
								[ ]byte( cardPin ) )
							server.cardHandler.Unlock( )
							break
						}
						server.cardHandler.Unlock( )
					}

					// notify
					_, _ = rw.Write( [ ]byte( "<html><body><h1>Success</h1><p>User " +
						user.Name +
						" added successfully with opad=" +
						string( user.OPad ) +
						", ipad=" +
						string( user.IPad ) +
						"</p><p>Card id: " +
						userCard.ID ) )

					// save
					_ = server.database.Save( )
				} else {
					rw.WriteHeader( http.StatusBadRequest )
				}
			} else {
				rw.WriteHeader( http.StatusBadRequest )
			}
			break
		}
	} else {
		// unauthorized
		rw.WriteHeader( http.StatusUnauthorized )
		fmt.Println( "[SERVER] a remote client (",
			hostname,
			"tried to access server new user page (denied)" )
		_, _ = rw.Write( [ ]byte( "You cannot access this interface" ) )
	}
}

// handle card assert
func ( server *Server ) handleCardRequest( rw http.ResponseWriter,
	request *http.Request ) {
	if err := request.ParseForm( ); err == nil {
		// extract card info
		cardID := ExtractFormValue( request,
			"id" )
		cardPassword := ExtractFormValue( request,
			"password" )

		// check session id
		if session, ok := server.sessionList[ request.Header.Get( authenticator.SessionIDHTTPHeader ) ]; ok {
			if card := server.database.FindCardByID( cardID ); card == nil {
				fmt.Println( "[SERVER - AUTH] can't find card with id",
					cardID)
				rw.WriteHeader( http.StatusUnauthorized )
			} else {
				if user := server.database.FindUserByUserID( card.UserID ); user != nil {
					if card.Password == cardPassword {
						rw.Header().Add(authenticator.OPadHTTPHeader,
							string(user.OPad))
						rw.Header().Add(authenticator.IPadHTTPHeader,
							string(user.IPad))
						rw.WriteHeader(http.StatusOK)
						session.loginStatus |= FlagSessionCardValid

						// notify
						fmt.Println( "[SERVER - AUTH] user",
							user.Name,
							" attempt a connection with valid card details (cardID=",
							cardID,
							", cardPassword=",
							cardPassword,
							") on session id",
							session.Id )
					} else {
						fmt.Println("[SERVER - AUTH] bad card password for user",
							user.Name,
							"login attempt (card content is not correct)" )
						rw.WriteHeader(http.StatusUnauthorized)
					}
				} else {
					fmt.Println( "[SERVER - AUTH] can't find user associated to card",
						card )
					rw.WriteHeader( http.StatusUnauthorized )
				}
			}
		} else {
			rw.WriteHeader( http.StatusUnauthorized )
		}
	} else {
		rw.WriteHeader( http.StatusBadRequest )
	}
}

// handle login request
func ( server *Server ) handleLoginRequest( rw http.ResponseWriter,
	request *http.Request ) {
	if err := request.ParseForm( ); err == nil {
		// extract card info
		login := ExtractFormValue( request,
			"login" )
		password := ExtractFormValue( request,
			"password" )

		// check session id
		if session, ok := server.sessionList[ request.Header.Get( authenticator.SessionIDHTTPHeader ) ]; ok {
			if user := server.database.TestLogUser( login,
				password,
				request.Header.Get( authenticator.OPadHTTPHeader ),
				request.Header.Get( authenticator.IPadHTTPHeader ) ); user != nil {
				fmt.Println( "[SERVER - AUTH] user",
					user.Name,
					"validated his login" )
				session.loginStatus |= FlagSessionLoginValid
			} else {
				fmt.Println( "[SERVER - AUTH] log failed for user with login",
					login,
					"and password",
					password )
				rw.WriteHeader( http.StatusUnauthorized )
			}
		}
	} else {
		rw.WriteHeader( http.StatusBadRequest )
	}
}
