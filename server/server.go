package server

import (
	"./database"
	"fmt"
	"gitlab.com/M2UCP/authentication/authenticator"
	"gitlab.com/M2UCP/authentication/smartcard"
	"math/rand"
	"net"
	"net/http"
	"strconv"
	"time"
)

type Server struct {
	// smart card handler
	cardHandler *card.Handler

	// token manager
	tokenManager *authenticator.TokenManager

	// configuration
	configuration *Configuration

	// is running?
	isRunning bool

	// session list (key is session id)
	sessionList map[ string ] *Session

	// database
	database *database.Database
}

// listen for incoming request
func ( server *Server ) listenAndServe( ) {
	_ = http.ListenAndServe( ":" +
		strconv.Itoa( server.configuration.Server.Port ),
		server )
}

// build server
func BuildServer( configuration *Configuration ) ( *Server, error ) {
	// allocate
	server := &Server{
		isRunning: true,
		configuration: configuration,
		sessionList: make( map[ string ] *Session ),
		database: database.BuildDatabase( configuration.Server.DatabasePath ),
	}

	// build card handler
	var err error
	if server.cardHandler, err = card.BuildHandler( ); err != nil {
		return nil, err
	}

	// build token manager
	server.tokenManager = authenticator.BuildTokenManager( server.cardHandler,
		authenticator.CardModeM2MServer )

	// start thread
	go server.update( )

	// listen for requests
	go server.listenAndServe( )

	// done
	return server, nil
}

// update sessions
func ( server *Server ) updateSession( ) {
	for sessionID, session := range server.sessionList {
		if time.Now( ).Sub( session.KeepAliveTime) >= time.Duration( server.configuration.Server.Session.KeepAlive ) * time.Second {
			// notify
			fmt.Println( "[SERVER - SESSION] session",
				sessionID,
				"has been inactive for",
				server.configuration.Server.Session.KeepAlive,
				"second(s)" )

			// close session
			session.Close( )

			// remove from map
			delete( server.sessionList,
				sessionID )

			// notify
			fmt.Println( "[SERVER - SESSION] session",
				sessionID,
				"is now removed" )
		}
	}
}

// update thread
func ( server *Server ) update( ) {
	for server.isRunning {
		// update smart cards
		server.cardHandler.Update( )

		// update token manager
		server.tokenManager.Update( )

		// update sessions
		server.updateSession( )

		// sleep
		time.Sleep( time.Millisecond * 16 )
	}
}

// serve http requests
func ( server *Server ) ServeHTTP( rw http.ResponseWriter,
	request *http.Request ) {
	switch request.URL.Path {
	case "/hello":
		if request.Method == "POST" {
			server.handleHelloRequest(rw,
				request)
		} else {
			rw.WriteHeader( http.StatusBadRequest )
		}
		break

	case "/brb":
		if request.Method == "POST" {
			server.handleBrbRequest( rw,
				request)
		} else {
			rw.WriteHeader( http.StatusBadRequest )
		}
		break

	case "/new":
		server.handleNewUserRequest( rw,
			request )
		break

	case "/card":
		if request.Method == "POST" {
			server.handleCardRequest(rw,
				request)
		} else {
			rw.WriteHeader( http.StatusBadRequest )
		}
		break

	case "/login":
		if request.Method == "POST" {
			server.handleLoginRequest( rw,
				request )
		} else {
			rw.WriteHeader( http.StatusBadRequest )
		}

	default:
		rw.WriteHeader( http.StatusBadRequest )
		break
	}
}

// generate random session id
func ( server *Server ) generateSessionId( ) string {
	// output
	output := ""

	// authorized characters
	const authorizedCharacterList = "azertyuiopqsdfghjklmwxcvbn12346567890"

	// generate
	for i := 0; i < server.configuration.Server.Session.IDLength; i++ {
		output += string( authorizedCharacterList[ rand.Int( ) % len( authorizedCharacterList ) ] )
	}

	// done
	return output
}

// add a session
// returns the session id
func ( server *Server ) addSession( remoteAddress string ) *Session {
	// split address
	hostname, _, _ := net.SplitHostPort( remoteAddress )

	// generate session id
	sessionID := server.generateSessionId( )

	// build session
	session := BuildSession( sessionID,
		hostname )

	// add session
	server.sessionList[ sessionID ] = session

	// notify
	fmt.Println( "[SERVER] adding new session",
		session )

	// add session id
	return session
}
