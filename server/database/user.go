package database

import (
	"gitlab.com/M2UCP/authentication/authenticator"
)

type User struct {
	// id
	ID string `json:"id"`

	// name
	Name string `json:"name"`

	// login
	Login string `json:"login"`

	// password (hmac)
	Password string `json:"password"`

	// ipad/opad
	IPad authenticator.HashPad `json:"ipad"`
	OPad authenticator.HashPad `json:"opad"`
}
