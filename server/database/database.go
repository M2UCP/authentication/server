package database

import (
	"encoding/json"
	"fmt"
	"gitlab.com/M2UCP/authentication/authenticator"
	"io/ioutil"
	"math/rand"
	"os"
)

const(
	UserDatabaseFile = "user.json"
	CardDatabaseFile = "card.json"

	UserIDLength = 16
)

var AuthorizedCharacterList = "azertyuiopqsdfghjklmwxcbn1234567890"

type Database struct {
	// database directory (must end with /)
	databaseDirectory string

	// user list
	userList [ ]*User

	// card list
	cardList [ ]*Card
}

// build database
func BuildDatabase( databaseDirectory string ) *Database {
	// allocate database
	database := &Database{
		databaseDirectory: databaseDirectory,
		cardList: make( [ ]*Card,
			0,
			1 ),
		userList: make( [ ]*User,
			0,
			1 ),
	}

	// notify
	fmt.Println( "[DATABASE] loading database..." )

	// create database directory
	_ = os.MkdirAll( databaseDirectory,
		0777 )

	// read user file content
	if fileContent, err := ioutil.ReadFile( databaseDirectory +
		UserDatabaseFile ); err == nil {
		// parse user file content
		_ = json.Unmarshal( fileContent,
			&database.userList )
		fmt.Println( "[DATABASE] loading",
			len( database.userList ),
			"users" )
	} else {
		fmt.Println( "[DATABASE] user database file missing, starting with no user..." )
	}

	// read card file content
	if fileContent, err := ioutil.ReadFile( databaseDirectory +
		CardDatabaseFile ); err == nil {
		// parse user file content
		_ = json.Unmarshal( fileContent,
			&database.cardList )
		fmt.Println( "[DATABASE] loading",
			len( database.cardList ),
			"cards" )
	} else {
		fmt.Println( "[DATABASE] card database file missing, starting with no card..." )
	}

	// done
	return database
}

// find user by id
func ( database *Database ) FindUserByUserID( userID string ) *User {
	// notify
	fmt.Println( "[DATABASE] looking for user with user id",
		userID )

	// look for user
	for _, user := range database.userList {
		if user.ID == userID {
			fmt.Println( "[DATABASE] found user",
				user.Name,
				"when looking for user with user id",
				userID )
			return user
		}
	}

	// notify
	fmt.Println( "[DATABASE] user id",
		userID,
		"gave no user result" )

	// not found
	return nil
}

// generate user id
func ( database *Database ) GenerateUserId( ) string {
	output := ""
	for {
		output = ""
		for i := 0; i < UserIDLength; i++ {
			output += string( AuthorizedCharacterList[ rand.Int( ) % len(AuthorizedCharacterList) ] )
		}
		if user := database.FindUserByUserID( output ); user == nil {
			break
		}
	}
	fmt.Println( "[DATABASE] generating random user ID",
		output )
	return output
}

// find card by id
func ( database *Database ) FindCardByID( cardID string ) *Card {
	fmt.Println( "[DATABASE] looking for card with id",
		cardID )
	for _, card := range database.cardList {
		if card.ID == cardID {
			if user := database.FindUserByUserID( card.UserID ); user != nil {
				fmt.Println("[DATABASE] found card belonging to user",
					card.UserID,
					"(",
					user.Name,
					") when looking for card id",
					cardID )
			} else {
				fmt.Println("[DATABASE] found card belonging to unknown user when looking for card id",
					cardID )
			}
			return card
		}
	}
	fmt.Println( "[DATABASE] can't find unknown card id",
		cardID )
	return nil
}

// generate card id
func ( database *Database ) GenerateCardID( ) string {
	output := ""
	for {
		output = ""
		for i := 0; i < authenticator.CardIDLength; i++ {
			output += string( AuthorizedCharacterList[ rand.Int( ) % len(AuthorizedCharacterList) ] )
		}
		if card := database.FindCardByID( output ); card == nil {
			break
		}
	}
	fmt.Println( "[DATABASE] generated random card id",
		output )
	return output
}

// add a user
func ( database *Database ) AddUser( name string,
	login string,
	password string ) *User {
	// create new user
	user := &User{
		Name: name,
		Login: login,
		ID: database.GenerateUserId( ),
	}

	// generate opad/ipad
	user.IPad.Generate( )
	user.OPad.Generate( )

	// save password
	user.Password = authenticator.HashDataDouble( password,
		user.IPad,
		user.OPad )

	// add user
	database.userList = append( database.userList,
		user )

	// notify
	fmt.Println( "[DATABASE] adding user",
		user )

	// done
	return user
}

// associate card to user
func ( database *Database ) AddCardForUser( userID string,
	cardPassword string ) *Card {
	// get user
	if user := database.FindUserByUserID( userID ); user != nil {
		// build card
		card := &Card{
			ID: database.GenerateCardID( ),
			Status: CardStatusWaiting,
			UserID: userID,
		}

		// hash password
		card.Password = cardPassword

		// add to card list
		database.cardList = append(database.cardList,
			card )

		// notify
		fmt.Println( "[DATABASE] added card",
			card,
			"for user",
			user )

		// done
		return card
	} else {
		return nil
	}
}

// save database
func ( database *Database ) Save( ) error {
	// save user
	if file, err := os.OpenFile( database.databaseDirectory +
			UserDatabaseFile,
		os.O_CREATE | os.O_WRONLY | os.O_SYNC | os.O_TRUNC,
		0744 ); err == nil {
		data, _ := json.MarshalIndent( database.userList,
			"",
			"\t" )
		_, _ = file.Write( data )
		_ = file.Close( )
	} else {
		return err
	}

	// save card list
	if file, err := os.OpenFile( database.databaseDirectory +
		CardDatabaseFile,
		os.O_CREATE | os.O_WRONLY | os.O_SYNC | os.O_TRUNC,
		0744 ); err == nil {
		data, _ := json.MarshalIndent( database.cardList,
			"",
			"\t" )
		_, _ = file.Write( data )
		_ = file.Close( )
	} else {
		return err
	}

	// notify
	fmt.Println( "[DATABASE] database saved" )

	// done
	return nil
}

// test login for user with hmac(login) hmac(password)
func ( database *Database ) TestLogUser( login string,
	password string,
	opad, ipad string ) *User {
	// look for hashed login
	for _, user := range database.userList {
		hashedLogin := authenticator.HashDataDouble( user.Login,
			authenticator.HashPad( ipad ),
			authenticator.HashPad( opad ) )

		if login == hashedLogin {
			if user.Password == password {
				return user
			}
		}
	}

	// not found
	return nil
}
