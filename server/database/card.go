package database

type CardStatus string
const(
	CardStatusWaiting = CardStatus( "waiting" )
	CardStatusInactive = CardStatus( "inactive" )
	CardStatusActive = CardStatus( "active" )
	CardStatusBlocked = CardStatus( "blocked" )
)

type Card struct {
	// card id
	ID string `json:"id"`

	// card password (8 bytes)
	Password string `json:"password"`

	// user ID which owns this card
	UserID string `json:"userID"`

	// current card status
	Status CardStatus `json:"status"`
}