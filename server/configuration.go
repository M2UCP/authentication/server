package server

import (
	"encoding/json"
	"errors"
	"io/ioutil"
)

const(
	ConfigurationFileName = "conf.json"
)

type Configuration struct {
	// server
	Server struct {
		// listening port
		Port int `json:"port"`

		Session struct {
			// session id string length
			IDLength int `json:"length"`

			// session keep alive (seconds)
			KeepAlive int `json:"keepAlive"`
		} `json:"session"`

		DatabasePath string `json:"databasePath"`
	} `json:"server"`
}

// build configuration
func BuildConfiguration( ) ( *Configuration, error ) {
	// allocate
	configuration := &Configuration{ }

	// read file
	if content, err := ioutil.ReadFile( ConfigurationFileName ); err == nil {
		if err = json.Unmarshal( content,
			configuration ); err == nil {
			// check
			if !configuration.check( ) {
				return nil, errors.New( "bad configuration file" )
			}
			return configuration, nil
		} else {
			return nil, err
		}
	} else {
		return nil, err
	}
}

// check configuration
func ( configuration *Configuration ) check( ) bool {
	if configuration.Server.Port <= 0 ||
		configuration.Server.Session.IDLength <= 0 ||
		configuration.Server.Session.KeepAlive <= 0 {
		return false
	} else {
		return true
	}
}
